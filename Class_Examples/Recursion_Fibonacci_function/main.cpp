/* 
 * File:   main.cpp
 * Author: Jorge Haro
 * Week 9 Fibonacci function.
 * Created on April 21, 2015, 10:44 AM
 */
//System Libraries 
#include <iostream>
#include <cstdlib>
using namespace std;
//Global Constants

//Function Prototypes
//Step 1: Describe the function.
//Step 2: Describe the Parameters.
//Step 3: Describe the Return Value.
/**
 * The fibb function calculates the Fibonacci sequence. Starting values 
 * are 0 and 1.
 * @param num the desired sequence.
 * @return Return the value at the wanted sequence.
 */
int fibb(int num);

//Execution Begins Here!
int main(int argc, char** argv) {
    int num;
    cout<<"Please enter a number: ";
    cin>>num;
    cout<<"Fib of: "<<num<< " is "<< fibb(num);
    
    //Exit Stage Left!
    return 0;
}

int fibb(int num){
    
    cout<<"Calculating Fib: "<<num<<endl;
    //Base case
    if(num==2)
        return 1;
    else if(num==1)
        return 0;
    else //Recursive call
        return fibb(num - 1 ) + fibb(num - 2);
}