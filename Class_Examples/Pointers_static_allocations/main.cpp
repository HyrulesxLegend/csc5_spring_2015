/* 
 * File:   main.cpp
 * Author: Jorge Haro
 * Created on May 14, 2015, 11:23 AM
 * Purpose: static allocation of pointers.
 *  */
#include <iostream>
#include <cstdlib>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    //Static pointers
    // & address of operator
    // * the pointer symbol
    int x = 42;
    int *p = &x;
    
    cout<< "X: " << x <<endl;
    cout<< "P: " << p <<endl;
    
    //Dereference 
    cout<< "*p: " << *p << endl;
    cout<< "Location of p: "<< &p <<endl; // or "address of"
    
    // Use the "new" keyword for allocation
    int *p1 = new int(12);
    cout<< "P1: " <<p1 <<endl;
    cout<< "*P1: " <<*p1 <<endl;
    
    // Delete deallocated memory
    // Delete does NOT REMOVE anything
    delete p1; 
    cout<<"Deleting p" <<endl;
    
    p1 = NULL; 
    cout<<"P1: " << p1 <<endl;
    cout<<"*P1: "<<*p1 <<endl;//Dangling pointer
    
    /*p1 = p;
    p1 = &x;
    //delete p1; //Can not delete static delete 
    p1 = new int(30);
    p1 = new int[2]; //Memory Leak
    */
    return 0;
}

