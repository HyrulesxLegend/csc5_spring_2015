/* 
 * File:   main.cpp
 * Author: Jorge Haro
 * Purpose: Write a Function that returns the number of even numbers in
 * a vector of ints.
 * Created on May 7, 2015, 9:41 AM
 */
#include <iostream>
#include <vector>
#include <cstdlib>
using namespace std;

int  numEvens(const vector<int>& v , int);
int main(int argc, char** argv) {
    return 0;
}

int numEvens(const vector<int>& v, int){
    int count = 0;  //counter variable to keep track of even numbers.
    for(int i= 0; i < v.size(); i++){
        if(v[i]%2==0){  // if it is equal to zero it's an even number.
            ++count;
        }
    }
    
     return count;
}