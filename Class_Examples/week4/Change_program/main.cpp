/* 
 * File:   main.cpp
 * Author: Jorge Haro
 * Created on March 12, 2015, 10:46 AM
 */
//System Libraries 
#include <iostream>
#include <cstdlib>
using namespace std;

//Execution Begins here!
int main(int argc, char** argv) {
    //Declare Variables
    cout<< "please enter owed amount: ";
    double owed;
    cin >> owed;
    
    cout <<"Please enter paid amount: ";
    double paid;
    cin>> paid;
    
    double change = paid - owed;
    //Convert to pennies
    //Add an offset
    
    int changeInt = (change + .0005) *100;
    
    // Get dollar amount
    int dollars = changeInt / 100;
    
    //Calculate remaining change
    changeInt %= 100; //changeInt % 100;
    
    //Quarters
    int quarters = changeInt / 25;
    changeInt %=25;
    
    //Dimes
    int dimes = changeInt/10;
    changeInt %=10;
    //Nickels
    int nickles = changeInt/5;
    changeInt %=5;
    //pennies
    int pennies = changeInt/1;
    changeInt %=1;
    
    cout<< "Dollars: " <<dollars <<endl;
    cout<< "Quarters: " <<quarters <<endl;
    cout<< "Dimes: " <<dimes <<endl;
    cout<< "Nickels: " <<nickles <<endl;
    cout<< "Pennies: " <<pennies <<endl;
    
    return 0;
}

