/* 
 * File:   main.cpp
 * Author: Jorge Haro
 * Created on March 5, 2015, 10:51 AM
 */
#include <iostream>
#include <cstdlib>
using namespace std;

int main(int argc, char** argv) {
    //Get a number from the user
    //Prompt the user
    cout<<"Please enter a number: ";
    int num;
    cin>>num;
    if (num >=0 ){
        cout<<num<<" is a Positive number";
    }else{
        cout<<num<<" is a Negative number!";
    }
    cout<<endl;
    
    //Check if even or odd 
    
    int rem = num%2;
    
    if (rem == 0){
        cout<<"and an Even number";
    }else {
        cout<<"and an Odd number";
    }
    return 0;
}

