/* 
 * File:   main.cpp
 * Author: Jorge Haro
 * Created on April 30, 2015, 10:32 AM
 */
#include <vector>
#include <cstdlib>
#include <iostream>
using namespace std;

int findNum(const vector<int>, int num);
void fillup(vector<int> & v);

int main(int argc, char** argv) {
    
    vector<int> v(2000);
    fillup(v);
    int loc= findNum(v,5);
    cout<<" found: "<<loc<<endl;

    return 0;
}

int findNum(const vector<int>& v, int num){
    for(int i=0; i<v.size(); i++){
        if(v[i]== num){
            return i;
        }
    }
    return -1;
}
void fillup(vector<int> & v){
    for (int i = 0; i < v.size(); i++){
        v[i] = rand()%2001;
    }
}