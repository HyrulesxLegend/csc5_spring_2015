/* 
 * File:   main.cpp
 * Author: Jorge Haro
 * Created on April 28, 2015, 10:36 AM
 */
#include <iostream>
#include <cstdlib>
using namespace std;

//Function prototypes
void computeCoin(int denom, int& numCoins, int& amountLeft);

//Execution Begins Here!
int main(int argc, char** argv) {
    cout<<"Enter a change amount: ";
    int num; 
    cin>> num;
    
    int numQuarters;
    int numDimes;
    int numNickels;
    int numPennies;

    computeCoin(25, numQuarters, num);
    computeCoin(10, numDimes, num);
    computeCoin(5, numNickels, num);
    computeCoin(1, numPennies, num);
    
    cout<<"Quarters: " <<numQuarters <<endl;
    cout<<"dimes: " <<numDimes <<endl;
    cout<<"Nickels: " <<numNickels<<endl;
    cout<<"Pennies: " <<numPennies<<endl;

    
    //Exit Stage Right!
    return 0;
}

/**
 * Compute coin calculates the number of coins of a specific denomination.
 * Reduces amount left by the denomination and number of coins.
 * @param denom The denomination
 * @param numCoins The amount of coins
 * @param amountLeft The reduced amount
 */

void computeCoin(int denom, int& numCoins, int& amountLeft){
    numCoins = amountLeft / denom; //going to give the number of coins.
    amountLeft &= denom; //going to give the remainder.
}
