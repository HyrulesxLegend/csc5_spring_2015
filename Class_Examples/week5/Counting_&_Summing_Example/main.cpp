/* 
 * File:   main.cpp
 * Author: Jorge Haro
 * Created on March 19, 2015, 10:13 AM
 */
#include <iostream>
#include <cstdlib>
using namespace std;

int main(int argc, char** argv) {
    //use while loops for counting*
    //ex
  /*  int count =0;
    cout<<"yes?";
    string answer;
    cin>>answer;
    while (answer =="yes"){
        ++count;
        cout<<"yes?";
        cin>>answer;
    }
   */
    
    //summing/totaling
    int sum = 0;
    cout<<"Enter a #, -1 to quit ";
    int num;
    cin>>num;
    while(num != -1)//user controlled loop
    {
        sum += num; sum = sum+num;
        cout<<"Prompt ";
        cin>>num;
    }
    return 0;
}

