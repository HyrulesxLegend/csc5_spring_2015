/* 
 * File:   main.cpp
 * Author: Jorge Haro  
 * Created on February 24, 2015, 11:40 AM
 */
//System Libraries
#include <iostream>
#include <cstdlib>
using namespace std;
// User Libraries

//Globals Constants

//Function Prototypes

//Execution Begins Here!
int main(int argc, char** argv) {
    //Declare Variables
    string name;
    cout<<" Hello, my name is Jorge! "<<endl;
    cout<<" What is your name?"<<endl;
    cin>>name;
    cout<<"Hello "<<name <<" I am glad to meet you."<<endl;
    //Exit Stage Right!
    return 0;
}

