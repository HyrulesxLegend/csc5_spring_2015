/* 
 * File:   main.cpp
 * Author: Jorge Haro
 * Created on April 28, 2015, 12:32 PM
 */
#include <iostream>
#include <vector>
#include <cstdlib>
using namespace std;
//Function Prototypes

void output(vector<int>& numbers);

//Execution Begins Here!
int main(int argc, char** argv) {
    vector<int>numbers;
    int num=0;
    cout<<"Enter numbers you want to add to the vector"<<endl; 
    while(num != -1){
        cin>>num;
        if(num != -1){
            numbers.push_back(num);
        }
        output(numbers);
    }
    return 0;
}
void output(vector<int>& numbers){
    for(int i= 0; i < numbers.size(); i++)
    {
    cout<<numbers[i]<<" ";
}cout<<endl;
}