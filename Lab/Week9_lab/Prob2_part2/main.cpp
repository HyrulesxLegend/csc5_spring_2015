/* 
 * File:   main.cpp
 * Author: Jorge Haro
 * Created on April 28, 2015, 11:47 AM
 */
//System Libraries
#include <iostream>
#include <vector>
#include <cstdlib>
using namespace std;
//Global Constants

//Function Prototypes

//Execution Begins Here!
int main(int argc, char** argv) {
    vector<int>numbers;
    int num=0;
    cout<<"Enter numbers you want to add to the vector"<<endl; 
    while(num != -1){
        cin>>num;
        if(num != 1){
            numbers.push_back(num);
        }
    }
    for(int i= 0; i < numbers.size(); i++){
        cout<<numbers[i]<<endl;
    }
    
    //Exit Stage Right!
    return 0;
}

