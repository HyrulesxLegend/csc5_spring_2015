/* 
 * File:   main.cpp
 * Author: Jorge Haro
 * Created on April 28, 2015, 11:36 AM
 */
#include <iostream>
#include <cstdlib>
#include <vector>

using namespace std;

//Execution Begins Here!
int main(int argc, char** argv) {
    //
    vector<int>v;
    v.push_back(3);
    v.push_back(6);
    v.push_back(8);
    
    //Output just for my reference 
    for (int i=0; i < v.size(); i++){
        cout<<v[i]<<endl;
    }
    //Exit Stage Right!
    return 0;
}

