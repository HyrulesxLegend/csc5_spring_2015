/* 
 * File:   main.cpp
 * Author: Jorge Haro
 * Created on February 26, 2015, 11:27 AM
 */
//System Libraries
#include <iostream>
#include <cstdlib>
using namespace std;
// User Libraries 

//Global Constants

//Function Prototypes

//Execution Begins Here!
int main(int argc, char** argv) {
    //Declare Variables
    
    int a = 5;
    int b = 10;
    int temp;
    
    cout << "a: " << a << " "  "" << " b: " << b << endl;
    
    temp=a;
    a=b;
    b=temp;
    
    cout << "a: " << a << " "  "" << "b: " << b << endl;

    
    //Exit Stage right!
    return 0;
}

