/* 
 * File:   main.cpp
 * Author: Jorge Haro
 * Created on February 26, 2015, 11:55 AM
 */
//System Libraries
#include <iostream>
using namespace std;
//User Libraries 

//Global Constants

//Function Prototypes

//Execution Begins Here!
int main(int argc, char** argv) {
    //Declare Variables
    int x;
    cout<<" enter a integer: "<<endl;
    cin>> x;
    x = x+5;
    cout<< x;
    
    //Exit Stage Right!
    return 0;
}

