/* 
 * File:   main.cpp
 * Author: Jorge Haro
 * Created on February 26, 2015, 11:30 AM
 */
//System Libraries
#include <iostream>
#include <cstdlib>
using namespace std;
// User Libraries 

//Global Constants

//Function Prototypes

//Execution Begins Here!
int main(int argc, char** argv) {
    //Declare Variables
    double meters, miles, feet, inches;
    cout<<" Please enter the Meters: ";
    cin>> meters;
    
    inches = meters* 39.370;
    feet = meters* 3.2808;
    miles = meters* 0.00062137;
    
    cout<< " Meters converted into Miles would be: "<<miles<<endl;
    cout<< " Meters to feet is: "<<feet<<endl;
    cout<< " Meters to inches is: "<<inches<<endl;
    
    
    
    //Exit Stage Right!
    return 0;
}

