/* 
 * File:   main.cpp
 * Author: Jorge Haro
 * Created on March 3, 2015, 11:26 AM
 */
//System Libraries
#include <iostream>
#include <iomanip>
using namespace std;
//User Libraries 

//Global Constants

//Function Prototypes

//Execution Begins Here!
int main(int argc, char** argv) {
    //Declare Variables
    int single, dub, trip, home, at_bats;
    
    cout<<"Please enter the number of singles, doubles, triples, homeruns "<<endl;
    cout<<"and at-bats for your slugging percentage(in that order!)"<<endl;
    cin>>single>>dub>>trip>>home>>at_bats;
    
    //Formula
     double slug_perc = static_cast<double>(single+2*dub+3*trip+4*home)/at_bats;
     
     cout<<"Your slugging percentage is "<<slug_perc<<endl;
     
    //Exit Stage Right!
    return 0;
}


