/* 
 * File:   main.cpp
 * Author: Jorge Haro
 * Created on March 3, 2015, 11:24 AM
 */
//System Libraries
#include <iostream>
#include <iomanip>
using namespace std;
//User Libraries 

//Global Constants

//Function Prototypes

//Execution Begins Here!
int main(int argc, char** argv) {
    //Declare Variables
    string var1 = "1";
    int var2 = 2;
    cout<<var1<<"+"<<var1<<"="<<var1 + var1<<endl;
    cout<<var2<<"+"<<var2<<"="<<var2+var2<<endl;
    
    //Exit Stage Right!
    return 0;
}

