/* 
 * File:   main.cpp
 * Author: Jorge Haro
 * Created on March 3, 2015, 12:14 PM
 */
//System Libraries
#include <iostream>
#include <iomanip>
using namespace std;
//User Libraries

//Global Constants

//Function Prototypes

//Execution Begins Here!
int main(int argc, char** argv) {
    //Declare Variables
    float amount, payment;
    double change;
    
    cout<<"Please enter the amount of purchase"<<endl;
    cin>>amount;
    cout<<"Please enter your payment amount"<<endl;
    cin>>payment;
    
    change = static_cast<double>(payment - amount);
    
    if (payment >= amount){
        cout<<"thats more than enough here's your change $"<<change;
    }else if(payment == amount){
        cout<<"Thank you For your purchase";
    }else{
        cout<<"Not enough money! Put your item back, you cannot afford it!";
    }
    //Exit Stage Right!
    return 0;
}

