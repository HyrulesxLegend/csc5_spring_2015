/* 
 * File:   main.cpp
 * Author: Jorge Haro
 * Not working properly!
 * Created on March 3, 2015, 11:40 AM
 */
//System Libraries
#include <iostream>
#include <iomanip>
using namespace std;
//User Libraries 

//Global Constants

//Function Prototypes

//Execution Begins Here!
int main(int argc, char** argv) {
    //Declare Variables
    int x, y;
    int temp;
    cout<<"Please enter two numbers less than 1000"<<endl;
    cin>>x>>y;
    cout<<"X = "<<x;
    cout<<"    Y = "<<y;
    
    temp=x;
    x=y;
    y=temp;

    cout<<setw(80)<<endl;
        cout<<setfill('-');
    cout<<" "<<endl;
    cout<<"X = "<<x;
    cout<<" Y = "<<y<<endl;
    
    
    //Exit Stage Right!
    return 0;
}

