/* 
 * File:   main.cpp
 * Author: Jorge Haro
 * Created on March 3, 2015, 11:58 AM
 */
//System Libraries
#include <iostream>
#include <iomanip>
using namespace std;
//User Libraries

//Global Constants

//Function Prototypes

//Execution Begins Here!
int main(int argc, char** argv) {
    //Declare Variables
    int x=5;
    int y=0;
    
    if (x==4){
        cout<<"y=4";
    }else if (x==9){
        cout<<"y=5";
    }else {
        cout<<"y=6";
    }
    
    //Exit Stage Right!
    return 0;
}

