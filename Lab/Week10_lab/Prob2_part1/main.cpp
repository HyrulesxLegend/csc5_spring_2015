/* 
 * File:   main.cpp
 * Author: Jorge Haro
 * Created on April 30, 2015, 11:42 AM
 */
#include <iostream>
#include <vector>
#include <cstdlib>
using namespace std;
//Function Prototypes
void fillup(vector<int>& v);
void output(const vector<int>& v);

//Execution Begins Here!
int main(int argc, char** argv) {
    vector<int> v(50);
    fillup(v);
    output(v);
    
    return 0;
}
void fillup(vector<int>& v){
    for(int i= 0; i< v.size(); i++){
        v[i]=rand()%51+50;
    }
}
void output(const vector<int>& v){
    for (int i= 0; i < v.size(); i++){
        cout<<v[i]<<endl;
    }
}