/* 
 * File:   main.cpp
 * Author: Jorge Haro
 * Created on May 07, 2015, 11:53 PM
 */
#include <iostream>
#include <vector>
#include <cstdlib>

using namespace std;
//Function Prototypes
bool findNum(vector<int>& v, int num);
void fillup(vector<int>& v);
void output(const vector<int>& v);
int duplicates(const vector<int>& v);

//Execution Begins Here!
int main(int argc, char** argv) {
    int num;
    vector<int> v(50);
    cout<<"Please enter a number: ";
    cin>>num;
    fillup(v);
    output(v);
    if(findNum(v, num)== true){
        cout<<"Found in Vector ";
    }else{
        cout<<"Not in the Vector ";
    }
    duplicates(v);
    
    return 0;
}
bool findNum(vector<int>& v, int num){// finds if number is in vector.
    for(int i = 0; i < v.size(); i++){
        if(v[i]== num){
            cout<<"Your number is in slot "<<i<<endl;
            return true;
        }
    }
    return false;
}
void output(const vector<int>& v){  //Display the vector with elements
    for (int i= 0; i < v.size(); i++){
        cout<<v[i]<<endl;
    }
}
void fillup(vector<int>& v){        //Filling up vector with random numbers.
    srand(time(0));
    for(int i= 0; i< v.size(); i++){
        v[i]=rand()%51+50;
    }
}
int duplicates(const vector<int>& v){
    int count = 0;  //counter variable to keep track of duplicate numbers.
    for(int i= 0; i < v.size(); i++){
        for(int j= 0; j< v.size(); i++){
            if(i==j){
                cout<<i<<" is a duplicate"<<endl;
            }
        }
    }
    
     return count;
}
