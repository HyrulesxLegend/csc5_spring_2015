/* 
 * File:   main.cpp
 * Author: Jorge Haro
 * Created on April 30, 2015, 12:08 PM
 */
#include <iostream>
#include <vector>
#include <cstdlib>
using namespace std;
//Function Prototypes
bool findNum(vector<int>& v, int num);
void fillup(vector<int>& v);
void output(const vector<int>& v);

//Execution Begins Here!
int main(int argc, char** argv) {
    int num;
    vector<int> v(50);
    cout<<"Please enter a number"<<endl;
    cin>>num;
    fillup(v);
    output(v);
    if(findNum(v, num)== true){
        cout<<"Found in Vector";
    }else{
        cout<<"Not in the Vector";
    }
    
    
    return 0;
}
bool findNum(vector<int>& v, int num){
    for(int i = 0; i < v.size(); i++){
        if(v[i]== num){
            cout<<" your number is in slot "<<i<<endl;
            return true;
        }
    }
    return false;
}
void output(const vector<int>& v){
    for (int i= 0; i < v.size(); i++){
        cout<<v[i]<<endl;
    }
}
void fillup(vector<int>& v){
    for(int i= 0; i< v.size(); i++){
        v[i]=rand()%51+50;
    }
}