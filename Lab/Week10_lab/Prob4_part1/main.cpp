/* 
 * File:   main.cpp
 * Author: Jorge Haro
 * Created on May 7, 2015, 11:32 AM
 */
//System Libraries
#include <iostream>
#include <vector>
#include <cstdlib>
using namespace std;
//Function Prototypes
int findNum(const vector<int>& v, int num);

//Execution Begins Here!
int main(int argc, char** argv) {
    int num;
    vector<int> v;
    
    cout<<"Hello Please enter a value you would like to check if present in";
    cout<<" vector"<<endl;
    cin>>num;
    findNum(v, num);
    cout<<"your number is in slot "<<findNum(v, num);
    
    
    //Exit Stage Right!
    return 0;
}

int findNum(const vector<int>& v, int num){
    for(int i= 0; i < v.size(); i++){
        if(v[i] == num){
            return i;
        }else{
            cout<<"not in vector"<<endl;
        }
    }
    return -1;
}