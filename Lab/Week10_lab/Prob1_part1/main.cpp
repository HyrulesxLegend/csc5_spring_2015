/* 
 * File:   main.cpp
 * Author: Jorge Haro
 * Created on April 30, 2015, 11:26 AM
 */
#include <iostream>
#include <vector>
#include <cstdlib>
#include <fstream>
using namespace std;
//Function Prototypes

//Execution Begins Here!
int main(int argc, char** argv) {
    cout<<"Enter a file name";
    string fileName;
    cin>>fileName;
    ifstream infile;
    infile.open(fileName.c_str());
    while (!infile){
        cout<<"Error, Enter another file name!";
        cin>>fileName;
        infile.open(fileName.c_str());
    }
    vector<int> v;
    for(int i = 0; i < v.size(); i++){
        v[i]+= fileName;
    }
    
    //Exit Stage Right!
    return 0;
}

