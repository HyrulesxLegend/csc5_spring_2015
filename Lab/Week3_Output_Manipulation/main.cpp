/* 
 * File:   main.cpp
 * Author: Jorge Haro
 * Purpose: Week3 lab 
 * Created on March 3, 2015, 9:59 AM
 */
//System Libraries
#include <iostream>
#include <iomanip>
//User Libraries

//Global Constants

//Function Prototypes

//Execution Begins Here!
using namespace std;
int main(int argc, char** argv) {
    //Prompt user for two numbers
    cout<<"Please enter two number: ";
    //Declare Variables
    int num1, num2;
    cin>>num1>>num2;
    
    //Use static cast to conver to double
    double avg = static_cast<double>(num1 + num2) / 2.0;
    //Left justify
    cout<<left;
    
    //Use set Precision
    cout<<setw(10)<<"Value 1";
    cout<<setw(10)<<"Value 2";
    
    cout<<endl;
    
    cout<<setw(10)<<num1;
    cout<<setw(10)<<num2;
    
    cout<<endl;
    
    //Use set fill
    cout<<setfill('%');
    cout<<setw(10)<<"Avg:"<<avg;
    
    //Exit Stage Right!
    return 0;
}

