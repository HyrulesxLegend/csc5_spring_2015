/* 
 * File:   main.cpp
 * Author: Jorge Haro
 * Purpose:
 * Created on February 24, 2015, 9:59 AM
 */
//System Libraries
#include <iostream>
#include <cstdlib>
using namespace std;
//User Libraries

//Global Constants

//Function Prototypes

//Execution Begins Here!
int main(int argc, char** argv) {
    //Declare Variables
    cout<<"Hello World "<<endl;
    string text = "Hello World Again";
    cout<<text<<endl;
    
    // Create an int variable
    int userInput;
    cout<<"Please enter a number."<<endl;
    cin>>userInput;
    cout<<"you Entered:" <<userInput;
    //Exit Stage Right!
    return 0;
}

