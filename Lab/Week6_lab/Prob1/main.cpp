/* 
 * File:   main.cpp
 * Author: Jorge Haro
 * Created on April 2, 2015, 11:28 AM
 */
//System Libraries
#include <iostream>
#include <cstdlib>
using namespace std;
//Global Constants

//Function Prototypes

//Execution Begins Here!
int main(int argc, char** argv) {
    //Declare Variables
    int num1=0, num2=22, num3=99, num4=75, num5=11, num6=41, num7=9, num8=6, num9=3, num10=4;
    cout<<"the values of 10 numbers are:"<<endl;
    cout<<num1<<endl<<num2<<endl<<num3<<endl<<num4<<endl<<num5<<endl<<num6<<endl
        <<num7<<endl<<num8<<endl<<num9<<endl<<num10;
    if(num1>num2&&num1>num3&&num1>num4&&num1>num5&&num1>num6&&num1>num7&&num1>num8&&num1>num9&&num1>num10){
        cout<< num1<<" is larger than "<<num2<<" "<<num3<<" "<<num4<<" "<<num5<<" "<<num6<<" "<<num7
            <<" "<<num8<<" "<<num9<<" "<<num10;
    }else if (num2>num1&&num2>num3&&num2>num4&&num2>num5&&num2>num6&&num2>num7&&num2>num8&&num2>num9&&num2>num10){
         cout<< num2<<" is larger than "<<num1<<" "<<num3<<" "<<num4<<" "<<num5<<" "<<num6<<" "<<num7
            <<" "<<num8<<" "<<num9<<" "<<num10;
    }else if (num3>num1&&num3>num2&&num3>num4&&num3>num5&&num3>num6&&num3>num7&&num3>num8&&num3>num9&&num3>num10){
         cout<< num3<<" is larger than "<<num1<<" "<<num3<<" "<<num4<<" "<<num5<<" "<<num6<<" "<<num7
            <<" "<<num8<<" "<<num9<<" "<<num10;
    }
    //Exit Stage 
    return 0;
}

