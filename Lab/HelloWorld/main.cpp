/* 
 * File:   main.cpp
 * Author: Jorge Haro
 * Purpose: to Create basic program to output "Hello World."
 * Created on February 24, 2015, 9:43 AM
 */
//System Libraries
#include <iostream> // Library/Directory allows user input and output
#include <cstdlib> 
using namespace std;//Where the origin of my library comes from
//User Libraries

//Global Constants

//Function Prototypes

//Execution Begins Here!
int main(int argc, char** argv) {  //int main is the starting point of program.
    //Declare Variables
    cout<<"Hello World"<<endl;
    //Exit Stage Right!
    return 0; //Flag for the OS.
}

