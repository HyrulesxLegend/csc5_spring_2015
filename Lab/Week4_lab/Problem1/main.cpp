/* 
 * File:   main.cpp
 * Author: Jorge Haro
 * Created on March 12, 2015, 11:32 AM
 */
//System Libraries
#include <iostream>
#include <cstdlib>
using namespace std;

int main(int argc, char** argv) {
    int letter;
    
    cout<<" Hello please enter your grade score between 1-100"<<endl;
    cin>> letter;
    if ( letter >=90){
        cout<<"You have an A!"<<endl;
    }else if (letter >=80){
        cout<<"You received a B!"<<endl;
    }else if (letter >=70){
        cout<<"You earned a C, not bad"<<endl;
    }else if (letter >=60){
        cout<<"You earned a D.. try harder next time!"<<endl;
    }else if (letter <= 59){
        cout<<"Thats an F.. sorry but you need to study"<<endl;
    }
    return 0;
}

