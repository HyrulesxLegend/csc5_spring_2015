/* 
 * File:   main.cpp
 * Author: Jorge Haro
 * Created on March 12, 2015, 12:03 PM
 */
//System Libraries
#include <iostream>
#include <cstdlib>
#include <string>
using namespace std;

//Execution Begins Here!
int main(int argc, char** argv) {
    //Declare Variables
    string letter;
    
    cout<<" Hello please enter your grade Letter you would like to see ranges"<<endl;
    cin>> letter;
    if (letter == "a+" || letter == "A+"){
        cout<<"A+ = 100+"<<endl;
    }else if (letter == "a" || letter == "A"){
        cout<<"A = 93-100"<<endl;
    }else if (letter == "a-" || letter == "A-"){
        cout<<"A- = 90-92.9"<<endl;
    }else if (letter == "b+" || letter == "B+"){
        cout<<"B+ = 87-89.9"<<endl;
    }else if (letter == "B" || letter == "b"){
        cout<<"B = 83-86.9 "<<endl;
    }else if (letter == "b-" || letter == "B-"){
        cout<<"B- = 80-82.9 "<<endl;
    }else if (letter == "c+" || letter == "c+"){
        cout<<"C+ = 77-79.9 "<<endl;
    }else if (letter == "C" || letter == "c"){
        cout<<"C = 73-76.9 "<<endl;
    }else if (letter == "c-" || letter == "C-"){
        cout<<"C- = 70-72.9"<<endl;
    }else if (letter == "D+" || letter == "d+"){
        cout<<"D+ =67-69.9 "<<endl;
    }else if (letter == "d" || letter == "D"){
        cout<<"D = 63-66.9"<<endl;
    }else if(letter == "D-" || letter == "d-"){
        cout<<"D- = 60-62.9"<<endl;
    }else if (letter == "F" || letter == "f"){
        cout<<"F = 0-59.9 "<<endl;
    }

    return 0;
}

