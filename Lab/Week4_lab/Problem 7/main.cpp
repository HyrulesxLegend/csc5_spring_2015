/* 
 * File:   main.cpp
 * Author: Jorge Haro
 * Created on March 19, 2015, 11:56 AM
 */
#include <iostream>
#include <cstdlib>
using namespace std;

//Execution Begins Here!
int main(int argc, char** argv) {
    //Declare Variables
    int guess, num;
    cout<<"Hello in this game you'll have to guess a random number from"
            " 0-100"<<endl;
    cout<<"Please Enter your guess: ";
    cin>>guess;
    cout<<endl;
    num = rand()%5;
    if ( guess == num){
        cout<<"Thats correct!"<<endl;
    }else if (guess <num){
        cout<<" Your guess is a bit lower than the number"<<endl;
    }else if (guess > num){
        cout<<" Your guess is a bit too high"<<endl;
    }
    
    return 0;
}

