/* 
 * File:   main.cpp
 * Author: Jorge Haro
 * Created on March 19, 2015, 11:33 AM
 */
//System Libraries
#include <iostream>
#include <cstdlib>
using namespace std;

//Execution Begins Here!
int main(int argc, char** argv) {
    //Declare Variables
    string grade;
    cout<<"Hello, Please enter a grade"<<endl;
    cin>>grade;
    if (grade == "A+"){
        cout<<" you have a 4.0"<<endl;
    }else if (grade == "A"){
        cout<<" you have a 4.0"<<endl;
    }else if (grade == "A-"){
        cout<<" you have a 3.7"<<endl;
    }else if (grade == "B+"){
        cout<<" you have a 3.3"<<endl;
    }else if (grade == "B"){
        cout<<" you have a 3.0"<<endl;
    }else if (grade == "B-"){
        cout<<" you have a 2.7"<<endl;
    }else {
        cout<<"Invalid grade"<<endl;
    }
    return 0;
}

