/* 
 * File:   main.cpp
 * Author: Jorge Haro
 * Created on March 12, 2015, 11:44 AM
 */
//System Libraries 
#include <iostream>
#include <cstdlib>
using namespace std;

//Execution Begins Here!
int main(int argc, char** argv) {
    //Declare Variables
    int x = 0;
    cout<<"Please enter a number ";
    cin>>x;
    while (x <10){
        cout << x <<endl;
        x++;
    }
    return 0;
}

