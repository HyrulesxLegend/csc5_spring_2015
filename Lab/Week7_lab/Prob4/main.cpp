/* 
 * File:   main.cpp
 * Author: Jorge Haro
 * Created on April 21, 2015, 11:24 AM
 */
//System Libraries
#include <iostream>
#include <cstdlib>
using namespace std;
//Global Constants
//Function Prototypes
void swap(double num1, double num2);

//Execution Begins Here!
int main(int argc, char** argv) {
    double num1, num2;
    num1 = 3;
    num2 = 2;
    double swap(double& num1, double& num2);
    cout<< "num 1 = "<<num1;
    cout<<" num 2 = "<<num2;
    return 0;
}

void swap(double& num1, double& num2){
    double temp;
   
    temp = num1;
    num1 = num2;
    num2 = temp;
}
