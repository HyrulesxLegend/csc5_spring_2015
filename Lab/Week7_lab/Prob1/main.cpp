/* 
 * File:   main.cpp
 * Author: Jorge Haro
 * Created on April 7, 2015, 9:45 AM
 */
#include <iostream>
#include <cstdlib>
#include <cmath>
using namespace std;
//Function Prototypes

double power(double num, int x){
    num = pow(num,x);
   
    return num;
} 
//Execution Begins Here!
int main(int argc, char** argv) {
    //Declare variables
    int x;
    double num;
    
    cout<<"hello please enter two numbers to the power of whatever"<<endl;
    cin>>num;
    cin>>x;
    cout<< power(num, x);
    
    return 0;
}

