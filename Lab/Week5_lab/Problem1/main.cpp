/*
* File: main.cpp
* Author: Jorge Haro
* Purpose: compute total interest, amount, and minimum payment for credit card
*/
//System Libraries
#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;

//Global Constants

//Function Prototypes

//Execution Begins Here!
int main(int argc, char** argv) {
    //Declare variables
    double intDue;//Interest due
    double total_due;//Total amount due
    double minPay; //minimum payment
    double balance;
    char ans;
    do{
    cout<<"Please enter account balance $:"<<endl;
    cin>>balance;
    if(balance <=1000){
       intDue = balance * .015;
        cout<<"your interest charge is $"<<intDue<<endl;
    }
    else if ( balance < 1000){
        intDue = balance * .01;
        cout<<"Your interest charge is $"<<intDue<<endl;
    } else 
 
    
    cout<<" Input 'Y' or 'N' to redo"<<endl;
    cin>>ans;
    }while (ans == 'Y' || ans == 'y');
    //Exit Stage Right!
    return 0;
}
