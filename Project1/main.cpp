/* 
 * File:   main.cpp
 * Author: Jorge Haro
 * Created on April 8, 2015, 7:55 PM
 * Purpose: Project 1. Create a casino with two games. Craps and Slots.
 */
//System Libraries
#include <iostream>
#include <string.h>
#include <iomanip>
#include <cstdlib>
#include <ctime>
using namespace std;
//Global Constants

//Function Prototypes
void clearScreen();
int slot1();
int slot2();
int slot3();
int slota();
int slotb();
int slotc();
int slotx();
int sloty();
int slotz();

//Execution Begins Here!
int main(int argc, char** argv) {
    //Declare Variables
    char choice;
    int token = 200;
    do{
        cout<<"Welcome to Mung's Indian Casino!"<<endl;
        cout<<"Here we have all sorts of games..well"<<endl;
        cout<<"only two really, but that doesn't mean its not fun!"<<endl;
        cout<<"You Start out with 200 Tokens on the house."<<endl;
        cout<<"Choose from either CRAPS or Slots."<<endl;
        cout<<"Each game will cost 25 Tokens to play."<<endl;
        cout<<"Enter 1 for Craps and Enter 2 for Slots."<<endl;
        cout<<"Tokens remaining: "<<token<<endl;
        int choice2;
    cin>>choice2;
    clearScreen();
    //Based upon the choice, display the results
    switch (choice2) {
        case 1:{
            
            token -=25; // Use 25 tokens to play.
            cout<<"You Chose Craps!"<<endl;
            cout<<"Get Ready to roll dice"<<endl;
                              //Craps Code
    //Set random number seed
    srand(time(0));
    
     //Declare Variables
     int die1=0;
     int die2=0;
     int roll=0;
     double win=0;
     int loss=0;
     int point=0;
     char ans;
     //Set Loop to repeat game when finished
     do{
     for(int game=1; game<3; game++){
         //Create dice
         die1=(rand()%6)+1;
         die2=(rand()%6)+1;
         roll = die1 + die2;
         cout<<die1<<" plus "<<die2<<" = " <<roll<<endl;
         
         if(roll == 7 || roll== 11)// Wins First round earns 5 tokens
         {
             win++;
             token+=5;
         }
         else if(roll == 2 || roll== 3 || roll == 12) //loses first round
         {
             loss++;  
         }
        else if(roll==4 || roll==5 || roll==6 || roll==8 || roll==9 || roll==10)
        {
            point=roll; //point set
            do{
                //Create dice again for next round
                die1=(rand()%6)+1;
                die2=(rand()%6)+1;
                roll = die1 + die2;
                cout<<die1 <<" plus "<<die2<<" = "<<roll<<endl;
                if(roll==point)// Wins in second round earn 2 tokens.
                {
                    win++;
                    token+=2;
                }
                else if(roll == 7)// Lose if Player rolls 7
                {
                    loss++;
                }
            }while(roll != point && roll != 7); 
            cout<<"End of Game: "<<game<<endl;
        }
     }    
     cout<<win<<" wins and " <<loss<< " loses"<<endl;
     cout<<"You have "<<token<<" tokens remaining"<<endl;
     cout<<"Would you like to play this game again?"<<endl;
     cin>>ans;
     clearScreen(); // Clears screen for new game
    }while(ans == 'y' || ans == 'Y');
     
            break;
        }
        case 2: {
             // Slots
            token-=25; //Use up 25 tokens to play
     //Declare Variables
            char ans;
            char choice;
            do{
       
    cout<<"Hello Welcome to the game, Slots!"<<endl;
    cout<<"There are 6 characters in this game:"<<endl;
    cout<<" 1, 2, 3, 4, 5, and 6."<<endl;
    cout<<"6 is cherry, while 3 is a wild."<<endl;
    cout<<" Player must match 3 symbols in the center row."<<endl;
    cout<<"Please Y or y to start gambling!"<<endl;
    cin>>choice;
    clearScreen();
    
    if (choice == 'y' || choice == 'Y')
    {
        cout<<"----------"<<endl;
        cout<<"  "<<slota()<<" ";
        cout<<slotb()<<" ";
        cout<<slotc()<<" ";
        cout<<endl;
        cout<<"  "<<slot1()<<" ";
        cout<<slot2()<<" ";
        cout<<slot3()<<" ";
        cout<<endl;
        cout<<"  "<<slotx()<<" ";
        cout<<sloty()<<" ";
        cout<<slotz()<<" ";
        cout<<endl;
        cout<<"----------"<<endl;
    }else {
        cout<<"incorrect input"<<endl;
    }if(slot1() != slot2() && slot1() != slot3())  
    {                                              
        cout<<"Lose!"<<endl;
    }
        if (slot1()== 6 && slota()!= 6 && slotx() != 6)
    {
        cout<<"You got a Cherry! you win!"<<endl;
        token+=20;
    }if (slot2()== 6 && slotb() != 6 && sloty() != 6)
    {
        cout<<"You got a Cherry! you win!"<<endl;
        token+=20;
    }if(slot3()== 6 && slotc() != 6 && slotz() != 6)
    {
        cout<<"You got a Cherry! you win!"<<endl;
        token+=20;
    }
    if (slot1()== 3 && slot2() == 3 && slot3() == 3)   //you get a wild
    {
        cout<<"Whoa! you got a wild!"<<endl;
        token+=40;
    }
    cout<<"Tokens: "<<token<<endl;
    cout<<"Play again? enter Y"<<endl;       
    cin>>ans;
    clearScreen();
    }while(ans == 'Y' || ans == 'y');
 
            
            break;
        }
        default: cout<<"Incorrect choice!"<<endl;
    }
      cout<<"Would you like to choose another game?"<<endl;
      cin>>choice;
      clearScreen();
    }while( choice == 'Y' || choice == 'y');
    //Exit Stage right!
    return 0;
}

//Functions

void clearScreen()
{
    for(int i = 0; i < 50; i++)
        cout<<endl;
}
int slot1()
{
    int slot_1 = rand()%6+1;
}
int slot2()
{
    int slot_2 = rand()%6+1;
}
int slot3()
{
    int slot_3 = rand()%6+1;
}
int slota()
{
    int slot_a = rand()%6+1;
}
int slotb()
{
    int slot_b = rand()%6+1;
}
int slotc()
{
    int slot_c = rand()%6+1;
}
int slotx()
{
    int slot_x = rand()%6+1;
}
int sloty()
{
    int slot_y = rand()%6+1;
}
int slotz()
{
    int slot_z = rand()%6+1;
}