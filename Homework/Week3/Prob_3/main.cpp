/* 
 * File:   main.cpp
 * Author: Jorge Haro
 * Created on March 5, 2015, 10:15 AM
 */
//System Libraries
#include <iostream>
#include <iomanip>
#include <string>
using namespace std;
//User Libraries 

//Global Constants

//Function Prototypes

//Execution Begins Here!
int main(int argc, char** argv) {
    //Declare Variables
    string name1, name2, name3;
    int quiz1,quiz2,quiz3,quiz4;
    double avg;
    cout<<"hello Please enter your names"<<endl;
    
    cout<<"name         "<<"Quiz 1      "<<"Quiz 2      "<<"Quiz 3      "<<"Quiz 4"<<endl;
    cout<<"----         "<<"------      "<<"------      "<<"------      "<<"------  "<<endl;
    cout<<name1<<"      "<<endl;
    cout<<name2<<"      "<<endl;
    cout<<name3<<"      "<<endl;
    
    
    //Exit Stage Right!
    return 0;
}

