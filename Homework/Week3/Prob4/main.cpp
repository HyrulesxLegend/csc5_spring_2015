/* 
 * File:   main.cpp
 * Author: Jorge Haro
 * Created on March 5, 2015, 10:50 AM
 */
//System Libraries
#include <iostream>
#include <cstdlib>
using namespace std;
//User Libraries

//Global Constants

//Function Prototypes

//Execution Begins Here!
int main(int argc, char** argv) {
    //Declare Variables
    string iName, yName, food, num , adj, color, animl;
    //Input User names
    cout<<"Hello this is Mad Lib game."<<endl;
    cout<<"Please enter your name."<<endl;
    cin>>yName;
    cout<<"Please enter your Instructors name"<<endl;
    cin>>iName;
    cout<<"Please enter a food"<<endl;
    cin>>food;
    cout<<"Please enter a number between 100 and 120."<<endl;
    cin>>num;
    cout<<"Please enter and adjective."<<endl;
    cin>>adj;
    cout<<"Please enter a color."<<endl;
    cin>>color;
    cout<<"Please enter an animal."<<endl;
    cin>>animl;
    cout<<endl;
    //Output story
    cout<<"Dear Instructor "<<iName<<","<<endl;
    cout<<endl;
    cout<<"I am sorry that I am unable to turn in my homework at this time. First,"<<endl;
    cout<<"I ate a rotten "<<food<<", which made me turn "<<color<<" and extremely ill. I"<<endl;
    cout<<"came down with a fever of "<<num<<". Next, my "<<adj<<" pet"<<endl;
    cout<<animl<<" must have smelled the remains of the "<<food<<" on my homework,"<<endl;
    cout<<"because he ate it I am currently rewriting my homework and hope you"<<endl;
    cout<<"will accept it late."<<endl;
    cout<<endl;
    cout<<"Sincerely,"<<endl;
    cout<<yName<<endl;
    //Exit stage Right!
    return 0;
}

