/* 
 * File:   main.cpp
 * Author: Jorge Haro
 * Created on March 3, 2015, 6:05 PM
 */
//System Libraries
#include <iostream>
using namespace std;
//User Libraries

//Global Constants

//Function Prototypes

//Execution Begins Here!
int main(int argc, char** argv) {
    //Declare Variables
    int x, y;
    int total;
    cout<<"Hello Please enter two integers to find their sum"<<endl;
    cin>>x>>y;
    
    total= x + y;
    
    cout<<x<<" + "<<y<<" = " <<total;
    cout << "This is the end of the program.\n";
    //Exit Stage Right!
    return 0;
}

