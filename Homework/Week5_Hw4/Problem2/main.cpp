/* 
 * File:   main.cpp
 * Author: Jorge Haro
 * Created on March 26, 2015, 9:50 PM
 */
//System Libraries
#include <iostream>
#include <cstdlib>
using namespace std;
//Global Constants

//Function Prototypes

//Execution Begins Here!
int main(int argc, char** argv) {
    //Declare Variables
int number, negative_sum=0, positive_sum=0, sum=0, average_neg=0, 
average_pos=0, average=0;
int count=0, positiveCount=0, negativeCount=0; 

srand (time(0));

number = rand()% 101 - 200; //not sure if this is the correct range.

cout << "Hello please input 10 whole numbers: "<<endl;
cout<<number;
cout<<endl;

for(int i=0;i<10;i++){


if (number >= 0 ){
     positive_sum += number;
     positiveCount++;  count++;
 } 

 else{
     negative_sum += number ; 
     negativeCount++; count++;
 }  
 }
if(positiveCount>0){
 average_pos = positive_sum / positiveCount;
}

if(negativeCount>0){ 
average_neg = negative_sum / negativeCount; 
}

sum = positive_sum + negative_sum;

if(count>0){
  average = sum / count;
} 

 cout << " The Total sum of positive numbers is " << positive_sum << " and the average of";
 cout<<" the positive numbers entered is ";
 cout << average_pos<< endl; 
 cout << "The Total sum of negative numbers is " << negative_sum << " and the average of";
 cout<<" the negative numbers entered is ";
 cout << average_neg << endl; 
 cout << "The Total sum of numbers entered is " << sum << " and its average is "<< average << endl;

    //Exit Stage Right!
    return 0;
}